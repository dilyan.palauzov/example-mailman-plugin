======================
Example Mailman Plugin
======================

This is an example plugin for Mailman meant to be a base for creating new
Mailman plugins. You can fork or clone this repo as a starting point, add
useful things and remove things you don't need.

Setup
=====

In order to start using this repo, start by cloning this repo to create a new
package::

  $ git clone https://gitlab.com/mailman/example-mailman-plugin working-plugin
  $ cd working-plugin

Then, update the Python packaging metadata in ``setup.py`` to something unique
for your plugin, here we are going to calling it ``working-plugin``::

  # setup.py
  from setuptools import setup, find_packages

  setup(
      name='working-plugin',
      version='0.0.1',
      packages=find_packages('.'),
      description='Example plugin for Mailman',
      maintainer='Mailman Developers',
      maintainer_email='mailman-developers@python.org',
      install_requires = [
          'mailman',
          'atpublic',
      ]
  )

Finally, change the name of the directory holding the code to the package
name::
  $ mv example_mailman_plugin working_plugin

**NOTE** : Python packages cannot have hyphen ``-`` in their names so we use an
underscore ``_`` in the directory name. Please make sure that the name is a
valid Python package name.

This should be enough to get you started on a working Plugin for Mailman Core
with basic bootstrapped code. You can install it in a Python virtualenv using
``pip``::

  $ python3 -m venv venv
  $ source venv/bin/activate
  # Install the plugin in current venv using `pip`.
  (venv) $ pip install -e .

Activate plugin in Mailman Core
===============================

In order to activate the plugin in Mailman Core, add the following config to
``mailman.cfg``::

  # Plugin configuration.
  [plugin.working_plugin]
  class: working_plugin.ExamplePlugin
  enabled: yes

**Note**: The name of the plugin is the Python import path to the plugin
 ``working_plugin`` and the ``class`` represents the import path to the `Plugin
 interface implementation`_.


Test the plugin is working
==========================

The current example plugin provides a couple of components to add to
Mailman. You can verify each of them individually.

Plugin
------

To verify that the plugin is recognized by Mailman::

  $ mailman shell
  Welcome to the GNU Mailman shell
  Use commit() to commit changes.
  Use abort() to discard changes since the last commit.
  Exit with ctrl+D does an implicit commit() but exit() does not.

  >>> config.plugins
  {'working_plugin': <working_plugin.ExamplePlugin object at 0x1034c4dc0>}


Pipelines
---------

Verify Mailman recognizes that this plugin provides a pipelines::

  >>> from pprint import pprint
  >>> pprint(config.pipelines)
  {'default-example-plugin': <working_plugin.pipelines.pipelines.MyExamplePipeline object at 0x10387c6a0>,
  'default-owner-pipeline': <mailman.pipelines.builtin.OwnerPipeline object at 0x10386c9d0>,
  'default-posting-pipeline': <mailman.pipelines.builtin.PostingPipeline object at 0x10386cfa0>,
  'virgin': <mailman.pipelines.virgin.VirginPipeline object at 0x10386ceb0>}

Styles
------

Verify that Mailman recognizes the style::

  >>> manager = getUtility(IStyleManager)
  >>> pprint(list(manager.styles))
  [<mailman.styles.default.LegacyAnnounceOnly object at 0x1034a3d90>,
   <mailman.styles.default.LegacyDefaultStyle object at 0x1034a3d60>,
   <working_plugin.styles.styles.MyPrivateStyle object at 0x1034a3f10>,
   <mailman.styles.default.PrivateDefaultStyle object at 0x1034a3f40>]


Rules
-----

Verify that Mailman recognizes the new Rule::

  >>> config.rules['example-rule']
  <working_plugin.rules.rules.ExampleRule object at 0x108074be0>

Handlers
--------

TODO: This example plugin doesn't currently provide a Handler.


.. _Plugin interface implementation: https://gitlab.com/mailman/example-mailman-plugin/-/blob/master/example_mailman_plugin/__init__.py#L13
