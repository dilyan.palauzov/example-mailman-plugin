from setuptools import setup, find_packages


setup(
    name='mailman-example-plugin',
    version='0.0.1',
    packages=find_packages('.'),
    description='Example plugin for Mailman',
    maintainer='Mailman Developers',
    maintainer_email='mailman-developers@python.org',
    install_requires = [
        'mailman',
        'atpublic',
    ]
)
