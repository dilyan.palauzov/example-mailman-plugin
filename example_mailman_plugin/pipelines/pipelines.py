from public import public
from mailman.interfaces.pipeline import IPipeline
from mailman.pipelines.base import BasePipeline
from zope.interface import implementer

# Pipelines are list of "handlers"
# https://docs.mailman3.org/projects/mailman/en/latest/src/mailman/handlers/docs/handlers.html

# The Pipelines has to be an implementation of the IPipeline inteface
# https://gitlab.com/mailman/mailman/-/blob/master/src/mailman/interfaces/pipeline.py#L63

# Here are some example of built-in pipelines that are used by default in Mailman.
# https://gitlab.com/mailman/mailman/-/blob/master/src/mailman/pipelines/builtin.py

@public
@implementer(IPipeline)
class MyExamplePipeline(BasePipeline):

    # The name of the plugin should be unique.
    name = 'default-example-pipeline'

    # Briefly describe the plugin so it can be shows in web frontends to users
    # when Choosing a style.
    description = 'The built-in example pipeline'

    # List of handlers, in the correct order, to be used for processing the
    # email.
    _default_handlers = (
        'to-outgoing',
    )
